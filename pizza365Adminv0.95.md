# Pizza 365 Restaurant
## 📄 Description
 - ### **Trang của Admin - Đặt hàng**
 ## ✨ Feature
 + *Đặt hàng*,
 ![DatHang](images/01_img_taodonhang.png)


 ## 🧱 Technology
 + Front-end:
    : 1.[Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
    : 2. Javascript
    : 3.Jquery 3
    : 4. Ajax
    : 5. Local Storage
    : 6. JSON
